package com.training.ChapterTwo;

public class MyInteger2 extends MyInteger  {

	private static final long serialVersionUID = -6939642172874144026L;

	public boolean isEven(MyInteger obj) {
		return obj.isEven();
	}
	
	public boolean isOdd(MyInteger obj) {
		return isOdd();
	}
	
	public boolean isPrime(MyInteger obj) {
		return obj.isPrime();
	}
}
