package com.training.ChapterTwo;

public class CustOrder extends Order {
	
	public CustOrder() {}

	public double ComputerPrice(double shipping, double handlingCharge) {
		return getTotalPrice() + shipping + handlingCharge;
	}
	public String toString() {
		return "Cust Name "+ getCustName() +" \ncust number "+ getCustNumber() +"\nQuantity "+ getQuantity() +
				"\nprice "+ getUnitPrice() +" \ntotal price "+ getTotalPrice();
	}
}
