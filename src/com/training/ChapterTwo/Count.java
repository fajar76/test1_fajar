package com.training.ChapterTwo;

public class Count {
	private String nilai;

	public String getNilai() {
		return nilai;
	}

	public void setNilai(String _nilai) {
		this.nilai = _nilai;
	}
	
	public Count() {}
	
	public Count(String bil) { this.nilai = bil;}
	
	public int lengthCounts() {
		return nilai.length();
	}
	
	private boolean isValidNumber(String n) {
		boolean isNumber = true;
		try {
			int h = Integer.parseInt(n);
		} catch (Exception e) {
			isNumber = false;
		}
		
		return isNumber;
	}
	
	public void getValues() {
		boolean isNumber = false;
		int b = 0;
		
		for(int j=0;j<nilai.length();j++) {
			String n = nilai.substring(j, j+1);
			isNumber = isValidNumber(n);
			
			if (isNumber){
				b++;
				
				System.out.println("Count["+ j +"] is "+ n);
			}
		}
	}
	
}
