package com.training.ChapterTwo;

public class MyInteger1 extends MyInteger  {
	private static final long serialVersionUID = 1L;

	public boolean isEven(int nilai) {
		setValue(nilai);
		
		return isEven();
	}
	
	public boolean isOdd(int nilai) {
		setValue(nilai);
		return isOdd();
	}
	
	public boolean isPrime(int nilai) {
		setValue(nilai);
		return isPrime();
	}

}
