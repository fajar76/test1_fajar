package com.training.ChapterTwo;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class MyInteger implements Serializable{
	private int value;
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int n) {
		this.value = n;
	}
	
	public MyInteger() {}
	
	public MyInteger(int nilai) {
		this.value = nilai;
	}
	
	public boolean isEven() {
		if (getValue()%2 != 1) return true;
		else return false;
	}
	
	public boolean isOdd() {
		if (getValue()%2 == 1) return true;
		else return false;
	}
	
	public boolean isPrime() {
		int b=0;
		for(int h=1;h<=getValue();h++) {
			if (getValue()%h == 0) {
				b=b+1;
			}
		}
		
		if (b==2)
			return true;
		else return false;
	}
	
	public boolean equal (Object obj) {
		if (obj == null) return false;
		if (!(obj instanceof MyInteger)) return false;
		else {
			MyInteger nO = (MyInteger) obj;
			return ((nO.isEven() && this.isEven()) ||  (nO.isOdd() && this.isOdd()) || (nO.isPrime() && this.isPrime()));
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

