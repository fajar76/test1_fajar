package com.training.ChapterTwo;

import java.util.Arrays;

public class MyActionChapter2 {
	
	public boolean isAnagram(String x1, String x2) {
		
		char s1[] = x1.toCharArray();
		char s2[] = x2.toCharArray();
		
		int g1 = s1.length;
		int g2 = s2.length;
		
		if (g1!=g2) return false;
		int c1[] = new int[256];
		Arrays.fill(c1, 0);
		int c2[] = new int[256];
		Arrays.fill(c2, 0);
		
		for(int j=0;j < g1 && j < g2; j++) {
			c1[s1[j]]++;
			c2[s2[j]]++;
		}
		boolean isAnagram = true;
		for (int g=0;g<256;g++) 
		{
			if (c1[g] != c2[g]) {isAnagram = false; break;} 
		}
		return isAnagram;
	}
	
	public static void main(String[] args) {
		//no 1
		Rectangle p1 = new Rectangle(4, 40);
		Rectangle p2 = new Rectangle(3.5, 39.5);
		Rectangle p = new Rectangle(2, 10);
		Rectangle p3 = new Rectangle();
		
		p1.setColor("Red");
		p2.setColor("Red");
		
		System.out.println("Rectangle Color = "+ p.getColor() +", Area = "+ p.getArea() +", Perimeter = "+p.getPerimeter());
		System.out.println("Rectangle 1 Color = "+ p1.getColor() +", Area = "+ p1.getArea() +", Perimeter = "+p1.getPerimeter());
		System.out.println("Rectangle 2 Color = "+ p2.getColor() +", Area = "+ p2.getArea() +", Perimeter = "+p2.getPerimeter());
		System.out.println("Rectangle 3 Color = "+ p3.getColor() +", Area = "+ p3.getArea() +", Perimeter = "+p3.getPerimeter());
		
		// no 5
		MyInteger1 n1 = new MyInteger1();
		MyInteger2 n2 = new MyInteger2();
		
		n1.setValue(533);
		n2.setValue(5190);
		
		System.out.println("Genap 1 ............. : "+ n1.isEven());
		System.out.println("Ganjil 1 ............ : "+ n1.isOdd());
		System.out.println("Primer 1 ............ : "+ n1.isPrime());
		System.out.println("Genap 2 ............. : "+ n2.isEven());
		System.out.println("Ganjil 2 ............ : "+ n2.isOdd());
		System.out.println("Primer 2 ............ : "+ n2.isPrime());
		
		System.out.println("n1 equal n2 ......... : "+ n1.equal(n2));
		System.out.println("nilai n1 ............ : "+ n1.getValue());
		System.out.println("nilai n2 ............ : "+ n2.getValue());

		// No. 9
		Count hit = new Count("123DF56K9U");
		System.out.println("len Count ........... : "+ hit.lengthCounts());
		hit.getValues();
		
		// No 11
		MyActionChapter2 bx = new MyActionChapter2();
		System.out.println("is Anagram............. : "+ bx.isAnagram("gardend", "rangedd"));
		
		//No 12
		Triangle sg = new Triangle(1, 1.5, 1);
		sg.setColor("Yellow");
		sg.setFilled(true);
		sg.getPerimeter();
		
		System.out.println(sg.toString());
		System.out.println("Area >> "+ sg.getArea() +" Perimeter >> "+ sg.getPerimeter() +" color "+ sg.getColor() +" filled "+ sg.isFilled());
		
		//No 16
		CustOrder od = new CustOrder();
		
		od.setCustName("Trial");
		od.setCustNumber("12345");
		od.setUnitPrice(30);
		od.setQuantity(3);
		
		System.out.println(od.toString());
		System.out.println("computer price >> " + od.ComputerPrice(1, 4));
	}

	
}
