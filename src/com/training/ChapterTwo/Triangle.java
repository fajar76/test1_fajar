package com.training.ChapterTwo;

import com.training.utility.GeometricObject;

public class Triangle extends GeometricObject {
	private double side1 = 1.0;
	private double side2 = 1.0;
	private double side3 = 1.0;
	
	public Triangle() {}
	
	public Triangle(double s1, double s2, double s3) {
		this.side1 = s1;
		this.side2 = s2;
		this.side3 = s3;
	}
	
	public double getArea() {
		return (0.5) * (this.side1*this.side2);
	}
	
	public double getPerimeter() {
		return side1 + side2 + side3;
	}
	
	@Override
	public String toString() {
		return "Triangle: side 1 = " + side1 + " side2 = " + side2 + " side3 = " + side3;
	}
}
