package com.training.ChapterTwo;

import java.io.Serializable;

public abstract class Order implements Serializable {
	private String custName;
	private String custNumber;
	private double quantity;
	private double unitPrice;
	
	public Order() {}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustNumber() {
		return custNumber;
	}

	public void setCustNumber(String custNumber) {
		this.custNumber = custNumber;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public double getTotalPrice() {
		return getUnitPrice()*getQuantity();
	}
	
}
