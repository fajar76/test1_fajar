package com.training.ChapterTwo;

public class Rectangle {
	private double width = 1; 
	private double height = 1;
	private String color = "FFFFF";
	
	public Rectangle() {}
	
	public Rectangle(double l, double t) {
		this.width = l;
		this.height = t;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double lebar) {
		this.width = lebar;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setHeight(double tinggi) {
		this.height = tinggi;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String warna) {
		this.color = warna;
	}
	
	public double getArea() {
		return (height*width);
	}
	
	public double getPerimeter() {
		return (2*(width+height));
	}
}
