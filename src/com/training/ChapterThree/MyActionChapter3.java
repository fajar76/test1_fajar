package com.training.ChapterThree;

import com.training.ChapterTwo.Triangle;

public final class MyActionChapter3 {

	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		MyCircle o1 = new MyCircle();
		MyCircle o2 = new MyCircle();
		Triangle sgt = new Triangle(10, 10, 15);
		Triangle sgt2 = new Triangle(10,13,25);
		CompareObj cpr = new CompareObj();
		
		/**
		 * No 1
		 */
		o1.setDiameter(14);
		o2.setDiameter(20);
		
		cpr.max(sgt, sgt2);
		
		cpr.max(o1, o2);
		
		/** *************
		 * No 7
		 */
		SimpleCalc calc = new SimpleCalc(15, 16);
		calc.toString();
		System.out.println("Add Operator         >> "+ calc.adds());
		System.out.println("Substract Operator   >> "+ calc.substract());
		System.out.println("Multipe Operator     >> "+ calc.muliple());
		System.out.println("Divide Operator      >> "+ calc.divide());
		
		/** *******
		* No. 18
		* BufferReader
		*/
		
		
		/**
		 * No 11
		 * TryToParseString
		 */
		TryToParseString ss = new TryToParseString();
		try {
			ss.inValidNumberFormat("2");
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**
		 * No 14
		 * TestGrade
		 */
		

	}

}
