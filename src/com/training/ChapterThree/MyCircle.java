package com.training.ChapterThree;

import com.training.utility.GeometricObject;

public class MyCircle extends GeometricObject {
	double pi = 3.14;
	private double side;
	private double diameter;
	
	public MyCircle() {
	}
	
	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public double perimeter() {
		if (getSide() == 0) return 0d;
		
		
		return (2 * pi * getSide());
	}
	
	public double area() {
		double jari = 0d;
		if (getDiameter() > 0) {
			jari = getDiameter()/2;
		} else if (getSide() > 0) {
			jari = getSide();
		} 
		
		return pi * Math.pow(jari, 2);
	}

}
