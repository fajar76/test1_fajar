package com.training.ChapterThree;

import com.training.ChapterTwo.Triangle;
import com.training.utility.GeometricObject;

public class CompareObj extends GeometricObject{

	public CompareObj() {}
	
	public static void max(Triangle obj, Triangle obj2) {
		if (obj.getArea() > obj2.getArea()) {
			System.out.println("Triangle 1 greather than Triangle 2");
		} else if (obj.getArea() < obj2.getArea()) {
			System.out.println("Triangle 2 greather than Triangle 1");
		} else {
			System.out.println("Both of Triangles are equal");
		}
	}
	
	public static void max(MyCircle obj, MyCircle obj2) {
		if (obj.area() > obj2.area()) {
			System.out.println("Circle 1 greather than Circle 2");
		} else if (obj.area() < obj2.area()) {
			System.out.println("Circle 2 greather than Circle 1");
		} else {
			System.out.println("Both of Circles are equal");
		}
	}

}
