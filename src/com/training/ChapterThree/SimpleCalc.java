package com.training.ChapterThree;

public class SimpleCalc {
	private double nilai1;
	private double nilai2;
	
	public double getNilai1() {
		return nilai1;
	}

	public void setNilai1(double nilai1) {
		this.nilai1 = nilai1;
	}

	public double getNilai2() {
		return nilai2;
	}

	public void setNilai2(double nilai2) {
		this.nilai2 = nilai2;
	}

	public SimpleCalc() {}

	public SimpleCalc(double x1, double x2) {
		this.nilai1 = x1;
		this.nilai2 = x2;
	}

	public double muliple() {
		return getNilai1() * getNilai2();
	}

	public double divide() {
		return getNilai1() / getNilai2();
	}
	
	public double adds() {
		return getNilai1() + getNilai2();
	}
	
	public double substract() {
		return getNilai1() - getNilai2();
	}
	
	public String toString() {
		return "Simple Calc : \nvalue 1 "+ getNilai1() +"\nValue 2 "+ getNilai2();
	}
}
