package com.training.ChapterOne;

public class ChapterOne {
	// no 1
	public double getFutureInvestmentValue(double investAmount, int years, double rate) {
		return Math.pow(investAmount * (1+rate/100), years*12);
    }
	
	// no 10
	public static int sumDigits(long n) {
		String s = String.valueOf(n);
		int result = 0;
		
		
		for(int o=0;o<s.length();o++) {
			result +=  Integer.parseInt(s.substring(o,o+1));
		}
		
		return result;
	}
	
	// no 11
	public static int reverse(int number) {
		String s = String.valueOf(number);
		String result = "";
		
		for(int i=s.length();i>0;i--) {
			result += s.substring( i-1, i );
		}
		
		return Integer.parseInt(result);
	}
	
	//no 7
	public static void pattern(int number) {
		String s = String.valueOf(number);
		
		System.out.println("********** Pattern I *************");
		for(int i=0;i<s.length();i++) {
			System.out.println(s.substring(0, i+1));
		}
		
		System.out.println("********** Pattern II *************");
		for(int i=s.length();i>0;i--) {
			System.out.println(s.substring(0, i));
		}
		
		String spasi = "";
		System.out.println("********** Pattern III *************");
		for(int i=0;i<s.length();i++) 
		{
			spasi = "";
			for(int k=1;k<=(s.length()-1)-i;k++) {
				spasi += " ";
			}
			System.out.println(spasi + s.substring(0, i+1));
		}
		
		//pat IV
		System.out.println("********** Pattern IV *************");
		for(int i=s.length();i>0;i--) {
			spasi = "";
			for(int k=1;k<=s.length()-i;k++) {
				spasi += " ";
			}
			System.out.println(spasi+s.substring(0, i));
		}
	}
	
	//No 8
	public static double sumGanjil(int range) {
		
		double hasil = 0;
		
		for(int i=1;i<range-1;i++) 
		{
			int atas = 0;int bawah = 0;
			
			if(i%2 != 0) atas = i;
			if ((i+2)%2 != 0) bawah = i+2;
			
			if (atas > 0 && bawah > 0) {
				hasil += ((double)atas/(double)bawah);
			}
		}
		return hasil;
	}
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ChapterOne obj = new ChapterOne();
		MyTriangle sgt = new MyTriangle();
		
		System.out.println("No 1 Future Investment  : "+ obj.getFutureInvestmentValue(2000, 2, 14.6));
		System.out.println("hasil no 10 ........... : "+ obj.sumDigits(12345));
		
		System.out.println("hasil no 11 ........... : "+ obj.reverse(12345));
		
		System.out.println("hasil no 07 ........... : ");
		obj.pattern(123456);
		
		System.out.println("HASIL no 8 ............ : " + obj.sumGanjil(100));
		
		
		//no 19
		double s1 = 13;
		double s2 = 10;
		double s3 = 10;
		
		if (sgt.isValid(s1, s2, s3)) {
			System.out.println("No 19 \nArea Of Triangle "+ sgt.area(s1, s2, s3));
		} else {
			System.out.println("No 19 \n invalid side(s) of Triangle .............!!!");
		}
	}

	
}
